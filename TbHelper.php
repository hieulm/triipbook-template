<?php

/**
 * Created by PhpStorm.
 * User: hieul
 * Date: 1/22/2016
 * Time: 4:59 AM
 */
class TbHelper
{
    public static function getK2Categories()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quoteName(array('name', 'ordering')))
            ->from($db->quoteName('#__k2_categories'))
            ->where($db->quoteName('trash') . '=' . $db->quote('0'))
            ->order($db->quoteName('ordering') . 'ASC');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }
}