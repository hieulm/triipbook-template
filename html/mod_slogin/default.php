<?php
/**
 * Social Login
 *
 * @version    1.7
 * @author        SmokerMan, Arkadiy, Joomline
 * @copyright    © 2012. All rights reserved.
 * @license    GNU/GPL v.3 or later.
 */

// No direct access.
defined('_JEXEC') or die('(@)|(@)');
?>
    <noindex>
        <?php if ($type == 'logout') : ?>

            <form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post"
                  id="login-form">

                <?php if (!empty($avatar)) : ?>
                    <div class="slogin-avatar">
                        <a href="<?php echo $profileLink; ?>" target="_blank">
                            <img src="<?php echo $avatar; ?>" alt="" id="slogin-avatar"/>
                        </a>
                    </div>
                <?php else: ?>
                    <img src="http://dummyimage.com/100/39b549/fff" alt="" id="slogin-avatar">
                <?php endif; ?>

                <div class="login-greeting">
                    <?php echo JText::sprintf('MOD_SLOGIN_HINAME', htmlspecialchars($user->get('name'))); ?>
                </div>
                <ul class="ul-jlslogin">
                    <?php if ($params->get('slogin_link_auch_edit', 1) == 1) { ?>
                        <li>
                            <a href="<?php echo JRoute::_('index.php?option=com_users&view=edit'); ?>"><?php echo JText::_('MOD_SLOGIN_EDIT_YOUR_PROFILE'); ?></a>
                        </li>
                    <?php } ?>
                    <?php if ($params->get('slogin_link_profile', 1) == 1) { ?>
                        <li>
                            <a href="<?php echo JRoute::_('index.php?option=com_slogin&view=fusion'); ?>"><?php echo JText::_('MOD_SLOGIN_EDIT_YOUR_SOCIAL_AUCH'); ?></a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="logout-button">
                    <input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGOUT'); ?>"/>
                    <input type="hidden" name="option" value="com_users"/>
                    <input type="hidden" name="task" value="user.logout"/>
                    <input type="hidden" name="return" value="<?php echo $return; ?>"/>
                    <?php echo JHtml::_('form.token'); ?>
                </div>
            </form>
        <?php else : ?>
            <?php if ($params->get('inittext')): ?>
                <div class="pretext">
                    <p><?php echo $params->get('inittext'); ?></p>
                </div>
            <?php endif; ?>
            <div class="slogin-clear"></div>
            <?php if ($params->get('pretext')): ?>
                <div class="pretext">
                    <p><?php echo $params->get('pretext'); ?></p>
                </div>
            <?php endif; ?>
            <?php if ($params->get('show_login_form')): ?>
                <!-- begin loginModal -->
                <div class="reveal small tb-modal" id="loginModal" data-reveal>
                    <button class="close-button" data-close aria-label="Close reveal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="tb-modal-logo">
                        <img src="img/modal-logo.png" alt="">
                    </div>
                    <div class="row">
                        <?php if (count($plugins)): ?>
                            <?php
                            foreach ($plugins as $link):
                                $linkParams = '';
                                if (isset($link['params'])) {
                                    foreach ($link['params'] as $k => $v) {
                                        $linkParams .= ' ' . $k . '="' . $v . '"';
                                    }
                                }
                                ?>
                                <div class="small-12 columns">
                                    <?php if ($link['plugin_name'] == "facebook"): ?>
                                        <a href="<?php echo JRoute::_($link['link']); ?>"
                                           class="button">Login with Facebook</a>
                                    <?php elseif ($link['plugin_name'] == "google"): ?>
                                        <a href="<?php echo JRoute::_($link['link']); ?>"
                                           class="button alert">Login with Google+</a>
                                    <?php else: ?>

                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <div class="small-12 columns text-center">
                            <span class="text-center ">OR</span>
                        </div>
                        <?php
                            $redirectUrl = '&return=' . urlencode(base64_encode("/triipbook"));
                            $finalUrl = JRoute::_('index.php', true, $params->get('usesecure')) . $redirectUrl;
                            echo $finalUrl;
                        ?>
                        <form action="<?php echo $finalUrl; ?>" class="tb-form clearfix" method="post">
                            <fieldset class="userdata">
                                <div class="small-12 columns">
                                    <label
                                        for="modlgn-username"><?php echo JText::_('MOD_SLOGIN_VALUE_USERNAME') ?></label>
                                    <input id="modlgn-username" type="text" name="username" class="inputbox" size="18"/>
                                </div>
                                <div class="small-12 columns">
                                    <label for="modlgn-passwd"><?php echo JText::_('JGLOBAL_PASSWORD') ?></label>
                                    <input id="modlgn-passwd" type="password" name="password" class="inputbox"
                                           size="18"/>
                                </div>
                                <div class="small-12 columns">
                                    <input type="checkbox" id="rememberCheckbox">
                                    <label for="rememberCheckbox">Remember me</label>
                                    <a href="#" class="forgot-pass">forgot password ?</a>
                                </div>
                                <div class="small-12 columns">
                                    <input type="submit" name="Submit" class="button success login-button"
                                           value="<?php echo JText::_('JLOGIN') ?>"/>
                                    <hr/>
                                </div>
                                <input type="hidden" name="option" value="com_users"/>
                                <input type="hidden" name="task" value="user.login"/>
                                <input type="hidden" name="return" value="<?php echo $return; ?>"/>
                                <?php echo JHtml::_('form.token'); ?>
                            </fieldset>

                        </form>
                        <div class="small-12 columns text-center">
                            Don't have account ? <a href="#" class="" data-toggle="registerModal"
                                                    aria-label="Close reveal">Sign up for free</a>
                        </div>
                    </div>
                </div>
                <!-- end loginModal-->
            <?php endif; ?>
        <?php endif; ?>
    </noindex>
<?php echo $jll; ?>