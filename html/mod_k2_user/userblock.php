<?php
/**
 * @version        2.6.x
 * @package        K2
 * @author        JoomlaWorks http://www.joomlaworks.net
 * @copyright    Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license        GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>

<div class="reveal small tb-modal" id="loginModal" data-reveal>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="tb-modal-logo">
        <img src="<?php echo JURI::base() . 'templates/' . $app->getTemplate() . '/'; ?>img/modal-logo.png" alt="">
    </div>
    <div class="row">
        <form action="<?php echo JURI::root(true); ?>/index.php" method="post">
            <input type="submit" name="Submit" class="button ubLogout" value="<?php echo JText::_('K2_LOGOUT'); ?>" />
            <input type="hidden" name="option" value="<?php echo $option; ?>" />
            <input type="hidden" name="task" value="<?php echo $task; ?>" />
            <input type="hidden" name="return" value="<?php echo $return; ?>" />
            <?php echo JHTML::_( 'form.token' ); ?>
        </form>
    </div>
</div>