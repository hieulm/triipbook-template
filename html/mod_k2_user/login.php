<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>
<div class="reveal small tb-modal" id="loginModal" data-reveal>
	<button class="close-button" data-close aria-label="Close reveal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
	<div class="tb-modal-logo">
		<img src="<?php echo JURI::base().'templates/'.$app->getTemplate().'/';?>img/modal-logo.png" alt="">
	</div>
	<div class="row">
		<div class="small-12 columns">
			<button class="button">Login with facebook</button>
		</div>
		<div class="small-12 columns">
			<button class="button alert">Login with Google+</button>
		</div>
		<div class="small-12 columns">
			<span>OR</span>
		</div>
		<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" class="tb-form clearfix" method="post" name="login" id="form-login">
			<div class="small-12 columns">
				<input type="text" id="modlgn_username" name="username" size="18">
			</div>
			<div class="small-12 columns">
				<input type="password" id="modlgn_passwd" name="<?php echo $passwordFieldName; ?>">
			</div>
			<?php if(JPluginHelper::isEnabled('system', 'remember')): ?>
			<div class="small-12 columns">
				<input type="checkbox" id="rememberCheckbox">
				<label for="rememberCheckbox">Remember me</label>
				<a href="<?php echo $resetLink; ?>" class="forgot-pass"><?php echo JText::_('K2_FORGOT_YOUR_PASSWORD'); ?></a>
			</div>
			<?php endif; ?>
			<div class="small-12 columns">
				<button type="submit button" name="button" class="button success login-button"><?php echo JText::_('K2_LOGIN') ?></button>
				<hr/>
			</div>
		</form>
		<div class="small-12 columns text-center">
			Don't have account ? <a href="#" class="" data-toggle="registerModal" aria-label="Close reveal"><?php echo JText::_('K2_CREATE_AN_ACCOUNT'); ?></a>
		</div>
	</div>
</div>

