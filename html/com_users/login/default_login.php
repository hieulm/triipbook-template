<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
?>
<div class="login <?php echo $this->pageclass_sfx?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	</div>
	<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	<div class="login-description">
	<?php endif; ?>

		<?php if ($this->params->get('logindescription_show') == 1) : ?>
			<?php echo $this->params->get('login_description'); ?>
		<?php endif; ?>

		<?php if (($this->params->get('login_image') != '')) :?>
			<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JTEXT::_('COM_USER_LOGIN_IMAGE_ALT')?>"/>
		<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	</div>
	<?php endif; ?>

	<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="form-horizontal mainlogin">
        <h1><?php echo JText::_('LOGIN_BAGOCO')?></h1>
	<?php
	jimport( 'joomla.application.module.helper' );
	$new_modules = JModuleHelper::getModules( 'social-login');
	$attribs['style'] = 'none';

	if (count(JModuleHelper::getModules('social-login'))):  
		foreach ($new_modules as $new_module){
	   		echo JModuleHelper::renderModule($new_module, $attribs);
		}
	endif;
	?>        
        <div class="contentlogin">
            <label class="lblContact"><?php echo JText::_('LOGIN_EMAIL_PHONE')?>:<span>*</span></label>
            <input type="text" name="username" id="username" class="txtContact validate-username" value="" required="" aria-required="true"/>
            <div class="clear"></div>
            
            <label class="lblContact"><?php echo JText::_('LOGIN_PASS')?>:<span>*</span></label>
            <input type="password" name="password" id="password" class="txtContact validate-password" value="" required="" aria-required="true"/>
            <div class="clear"></div>
            
            <label class="lblContact"></label>
            <?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
            <input type="checkbox" name="remember" class="checBox" value="yes"/><span><?php echo JText::_('LOGIN_REMEMBER_ME') ?></span><br />
            <?php endif; ?>
            <a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>"><?php echo JText::_('LOGIN_RESET'); ?></a>
            <div class="clear"></div>
            
            <label class="lblContact"></label>
            <input type="submit" class="btnContact" value="<?php echo JText::_('LOGIN_LOGIN_BTN')?>"/>
            <?php $link = JRoute::_('index.php?option=com_users&view=registration&Itemid=141', false)?>
            <?php echo JText::sprintf('LOGIN_OR_SIGNUP_NOW', $link)?>
     
        
            <?php if ($this->tfa): ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $this->form->getField('secretkey')->label; ?>
					</div>
					<div class="controls">
						<?php echo $this->form->getField('secretkey')->input; ?>
					</div>
				</div>
			<?php endif; ?>

			<input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
</div>