<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>

<h1>Lorem ipsum dolor.</h1>

<div class="wapright latestnews <?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">
	<?php if(count($items)): ?>
    <?php foreach ($items as $key=>$item):
    	$shortDesc = strip_tags($item->introtext);
    	if(strlen($shortDesc) > 160){
    		$shortDesc = mb_substr($shortDesc, 0, 160)."...";
    	}
    ?>
	<div class="news">
        <h3><a style="color:inherit" href="<?php echo $item->link; ?>"><?php echo $item->title?></a></h3>
        <p><?php echo $shortDesc?>&nbsp;<a href="<?php echo $item->link; ?>" class="more"><?php echo JText::_('READ_MORE'); ?></a></p>
	</div>
		<?php endforeach;?>
	<?php endif;?>
</div>