<?php defined('_JEXEC') or die ('Restricted access'); ?>
<?php

$param = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

$path = JURI::base(true) . '/templates/' . $app->getTemplate() . '/';
$page_title = $doc->getTitle(); // get current page title

// get all K2 categories
function getK2Categories()
{
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select($db->quoteName(array('name', 'ordering')))
        ->from($db->quoteName('#__k2_categories'))
        ->where($db->quoteName('trash') . '=' . $db->quote('0'))
        ->order($db->quoteName('ordering') . 'ASC');
    $db->setQuery($query);
    $results = $db->loadObjectList();
    return $results;
}

// check if a page is front page or not
function isFrontPage()
{
    $is_front_page = false;
    $menu = $app->getMenu();
    if ($menu->getActive() == $menu->getDefault()) {
        $is_front_page = true;
    }
//  if(JRequest::getVar('view') == 'frontpage'){
//    $is_front_page = false;
//  }
    return $is_front_page;
}

// get current site url with current language
function getCurrentSiteUrl()
{
    // get baseurl with current language
    $current_base_url = $this->baseurl;
    if ($this->language == "en-gb") {
        $current_base_url .= '/en';
    }
    if ($this->language == 'vi-vn') {
        $current_base_url .= '/vi';
    }
    return $current_base_url;
}

$user = JFactory::getUser();
$isGuest = true;
if (!$user->guest) {
    $isGuest = false;
} else {
    $isGuest = true;
}

class TbHelper{

}

?>
