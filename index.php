<?php defined('_JEXEC') or die('Restricted access');

$params = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;
$path = JURI::base(true) . '/templates/' . $app->getTemplate() . '/';
$page_title = $doc->getTitle(); // get current page title
$user = JFactory::getUser();
$menu = $app->getMenu();
$lang = JFactory::getLanguage();
// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

$currentUrl = $_SERVER["REQUEST_URI"];
$redirectUrl = urlencode(base64_encode($currentUrl));
$redirectUrl = '&return=' . $redirectUrl;
$joomlaLoginUrl = 'index.php?option=com_users&view=login&Itemid=140';
$returnUrl = JRoute::_($joomlaLoginUrl . $redirectUrl, false);
$this->setGenerator(null);

//get k2 categories
$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select($db->quoteName(array('name', 'ordering')))
    ->from($db->quoteName('#__k2_categories'))
    ->where($db->quoteName('trash') . '=' . $db->quote('0'))
    ->order($db->quoteName('ordering') . 'ASC');
$db->setQuery($query);
$k2Cats = $db->loadObjectList();
// check if a page is front page or not
$is_front_page = false;
$menu = $app->getMenu();
if ($menu->getActive() == $menu->getDefault()) {
    $is_front_page = true;
}
// get current site url with current language
$current_base_url = $this->baseurl;
if ($this->language == "en-gb") {
    $current_base_url .= '/en';
}
if ($this->language == 'vi-vn') {
    $current_base_url .= '/vi';
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--    TODO: uncomment these lines-->
    <!--    --><?php //if (JRequest::getVar('lang', 'vi') == 'vi'): ?>
    <!--        <script type="text/javascript"-->
    <!--                src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&language=vi"></script>-->
    <!--    --><?php //else: ?>
    <!--        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>-->
    <!--    --><?php //endif; ?>


    <link rel="stylesheet" href="<?php echo $path ?>css/foundation-icons.css">
    <link rel="stylesheet" href="<?php echo $path ?>css/app.css">
    <link rel="stylesheet" href="<?php echo $path ?>css/style.css">
    <link rel="stylesheet" href="<?php echo $path ?>css/template.css">
</head>

<body>

<div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-right" id="offCanvas" data-off-canvas data-position="right">
            <ul class="vertical menu main-menu">
                <li class="menu-text" data-toggle="offCanvas">
                    <button class="" type="button" data-toggle="offCanvas">
                        <i class="fi-x"></i>
                    </button>
                </li>
                <li><a href="<?php echo JURI::base() . "news" ?>"><?php echo JText::_('TPL_TB_NEWS'); ?></a></li>
                <li><a href="#"><?php echo JText::_('TPL_TB_PLANING'); ?></a></li>
                <li><a href="#" data-open="registerModal"><?php echo JText::_('TPL_TB_SIGNUP'); ?></a></li>
                <li><a href="#" data-open="loginModal"><?php echo JText::_('TPL_TB_SIGNIN'); ?></a></li>
            </ul>
        </div>
        <div class="off-canvas-content" data-off-canvas-content>
            <jdoc:include type="modules" name="debug"/>
            <!-- #header -->
            <div id="header">
                <!-- top-nav here -->
                <!-- front-page navigation -->
                <!--                --><?php //if ($menu->getActive() == $menu->getDefault()): ?>
                <div class="navigation">
                    <!-- TitleBar for mobile screen-->
                    <div class="title-bar show-for-small-only">
                        <div class="title-bar-left">
                            <a href="<?php echo JURI::base(); ?>" class="brand title-bar-title"></a>
                        </div>
                        <div class="title-bar-right">
                            <button class="menu-icon" type="button" data-toggle="offCanvas"></button>
                        </div>
                    </div>
                    <!-- top-bar for desktop screen -->
                    <div class="top-bar hide-for-small-only">
                        <div class="top-bar-left">
                            <ul class="menu">
                                <li class="menu-text">
                                    <a href="<?php echo JURI::base(); ?>" class="brand"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="top-bar-right">
                            <ul class="medium-horizontal menu hide-for-small-only main-menu dropdown"
                                data-dropdown-menu>
                                <li>
                                    <a href="<?php echo JURI::base() . "news" ?>"><?php echo JText::_('TPL_TB_NEWS'); ?></a>
                                </li>
                                <li><a href="#"><?php echo JText::_('TPL_TB_PLANING'); ?></a></li>
                                <?php if (!$user->guest): ?>
                                    <!--TODO: user menu-->
                                    <li class="username-menu-item">
                                        <a href="#"><?php echo $user->name; ?></a>
                                    </li>
                                    <!--TODO: user dropdown menu here-->
                                    <li class="usericon-menu-item">
                                        <a href="<?php echo $profileLink; ?>" class="icon-button"
                                           data-toggle="menu-user-dropdown"
                                           aria-expanded="false" aria-controls="">
                                            <?php
                                            if (JPluginHelper::isEnabled('slogin_integration', 'profile') && $user->id > 0) {
                                                require_once JPATH_BASE . '/plugins/slogin_integration/profile/helper.php';
                                                $profile = plgProfileHelper::getProfile($user->id);
                                                $avatar = isset($profile->avatar) ? $profile->avatar : '';
                                                $profileLink = isset($profile->social_profile_link) ? $profile->social_profile_link : '';
                                            } else if (JPluginHelper::isEnabled('slogin_integration', 'slogin_avatar') && $user->id > 0) {
                                                require_once JPATH_BASE . '/plugins/slogin_integration/slogin_avatar/helper.php';
                                                $path = Slogin_avatarHelper::getavatar($user->id);
                                                if (!empty($path['photo_src'])) {
                                                    $avatar = $path['photo_src'];
                                                    if (JString::strpos($avatar, '/') !== 0)
                                                        $avatar = '/' . $avatar;
                                                }
                                                $profileLink = isset($path['profile']) ? $path['profile'] : '';
                                            }
                                            ?>
                                            <?php if (!empty($avatar)) : ?>
                                                <img src="<?php echo $avatar; ?>" alt="" id="slogin-avatar"/>
                                            <?php else: ?>
                                                <img src="http://dummyimage.com/100/39b549/fff" alt=""
                                                     id="slogin-avatar">
                                            <?php endif; ?>

                                        </a>
                                        <ul class="menu">
                                            <li><a href="#">Your trips</a></li>
                                            <li><a href="#">Wishlist</a></li>
                                            <li><a href="#">Account Info</a></li>
                                            <li><a href="#">Account Settings</a></li>
                                            <li>
                                                <a href="#">
                                                    <form
                                                        action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>"
                                                        method="post" id="login-form">
                                                        <input type="submit" name="Submit" class="tb-submit-btn"
                                                               value="<?php echo JText::_('JLOGOUT'); ?>"/>
                                                        <input type="hidden" name="option" value="com_users"/>
                                                        <input type="hidden" name="task" value="user.logout"/>
                                                        <input type="hidden" name="return"
                                                               value="<?php echo $return; ?>"/>
                                                        <?php echo JHtml::_('form.token'); ?>
                                                    </form>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php else: ?>
                                    <!--TODO: guest menu-->
                                    <li><a href="#"
                                           data-open="registerModal"><?php echo JText::_('TPL_TB_SIGNUP'); ?></a>
                                    </li>
                                    <li><a href="#"
                                           data-open="loginModal"><?php echo JText::_('TPL_TB_SIGNIN'); ?></a>
                                    </li>
                                <?php endif; ?>

                            </ul>
                        </div>
                    </div>
                </div>
                <section class="hero">
                    <div class="row intro">
                        <div class="small-centered medium-centered large-centered medium-12 large-12 columns">
                            <h1>Plan Your Trip And Travel</h1>
                            <p>with triipbook</p>
                        </div>
                    </div>
                    <div class="tb-search">
                        <div class="row">
                            <!-- large screen search-bar -->
                            <div
                                class="small-10 small-centered text-center columns show-for-large row large-collapse">
                                <div class="small-12 medium-3 columns">
                                    <button class="dropdown button"
                                            data-toggle="search-dropdown"><? echo array_values($k2Cats)[0]->name; ?>
                                    </button>
                                </div>
                                <div class="small-12 medium-7 columns">
                                    <input type="text" class="tb-search-input" placeholder="I'm looking for ... ">
                                </div>
                                <div class="small-12 medium-2 columns">
                                    <a class="success button" href="search.html">Search</a>
                                </div>
                            </div>
                            <!-- small and medium screen search-bar -->
                            <div
                                class="small-12 medium-8 small-centered text-center columns hide-for-large row small-collapse">
                                <div class="small-9 columns">
                                    <input type="text" input="text" data-open="searchModal">
                                </div>
                                <div class="small-3 columns">
                                    <a class="button success" data-open="searchModal">Search</a>
                                </div>
                            </div>
                        </div>

                        <!-- search-dropdown -->
                        <div class="dropdown-pane" id="search-dropdown" data-dropdown data-auto-focus="true">
                            <ul class="menu dropdown vertical" data-toggle="search-dropdown">
                                <? foreach ($k2Cats as $result): ?>
                                    <li><a href="#"><? echo $result->name; ?></a></li>
                                <? endforeach; ?>
                            </ul>
                        </div>

                    </div>
                </section>
                <!-- alternative navigation -->

            </div>

            <!-- #siteContent -->
            <div id="siteContent">
                <!--TODO: test if user is logged in-->
                <div id="tbDebug">
                    <h1>Debug</h1>
                    <?php
                    if (!$user->guest) {
                        echo 'You are logged in as:<br />';
                        echo 'User name: ' . $user->username . '<br />';
                        echo 'Real name: ' . $user->name . '<br />';
                        echo 'User ID  : ' . $user->id . '<br />';
                        echo 'Avatar : ' . $user->avatar . '<br/>';
                    } else {
                        echo "You are Guest";
                    }

                    ?>
                </div>



                <div class="tb-intro text-center">
                    <h1>Explore The World</h1>
                    <p>See where people are traveling, around the world</p>
                </div>

                <jdoc:include type="component"/>


            </div>
            <!-- #footer -->
            <div class="tb-footer-bottom">
                <div class="row full-width">
                    <div class="large-4 large-push-8 columns text-center large-text-right">
                        <ul class="home-social">
                            <li><a class="" href="#"><i class="fi-social-facebook"></i></a></li>
                            <li><a class="" href="#"><i class="fi-social-android"></i></a></li>
                            <li><a class="" href="#"><i class="fi-social-apple"></i></a></li>
                            <li><a class="" href="#"><i class="fi-social-windows"></i></a></li>
                        </ul>
                    </div>
                    <div class="large-8 large-pull-4 columns text-center large-text-left">
                        <div class="wrapper tb-centered">
                            <ul class="tb-links">
                                <li><a href="">English</a></li>
                                <li><a href="">Terms<span class="show-for-medium"> &amp; Condition</span></a></li>
                                <li><a href="">Contact</a></li>
                                <li><a href="">Career</a></li>
                                <li><a href="">Help</a></li>
                            </ul>
                            <p class="copyright text-center large-text-left">&copy; 1998&ndash;2015 Triipbook. All
                                rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- exit overlay -->
        <a class="exit-off-canvas"></a>
    </div>
</div>

<!-- modals -->
<!-- searchModal -->
<div class="reveal small tb-modal" id="searchModal" data-reveal>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="row">
        <div class="small-12 columns text-center">
            <button class="dropdown button"
                    data-toggle="search-dropdown-mobile"><? echo array_values($k2Cats)[0]->name; ?></button>
            <div class="dropdown-pane" id="search-dropdown-mobile" data-dropdown data-auto-focus="true">
                <ul class="menu dropdown vertical" data-toggle="search-dropdown-mobile">
                    <? foreach ($k2Cats as $result): ?>
                        <li><a href="#"><? echo $result->name; ?></a></li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="small-12 columns">
            <input type="text" class="tb-search-input" placeholder="I'm looking for ... ">
        </div>
        <div class="small-12 columns text-center">
            <a class="success button" href="search.html">Search</a>
        </div>
    </div>
</div>

<!-- loginModal -->
<jdoc:include type="modules" name="user_login"/>

<!-- registerModal -->
<div class="reveal small" id="registerModal" data-reveal>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="tb-modal-logo">
        <img src="<?php echo JURI::base() . 'templates/' . $app->getTemplate() . '/'; ?>img/modal-logo.png" alt="">
    </div>
    <div class="row">
        <div class="small-12 columns">
            <button class="button">Sign up with facebook</button>
        </div>
        <div class="small-12 columns">
            <button class="button alert">Sign up with Google+</button>
        </div>
        <div class="small-12 columns">
            <button class="button secondary" data-toggle="registerWithEmailModal">Sign up with Email</button>
        </div>
        <div class="small-12 columns text-center">
            By signing up, I agree to triipbook's <a href="#">Term of Service and Privacy Policy.</a>
        </div>
        <div class="small-12 columns text-center">
            Already a triipbook member ? <a href="#" data-toggle="loginModal">Login</a>
        </div>
    </div>
</div>
<!-- registerWithEmailModal -->
<div class="reveal small" id="registerWithEmailModal" data-reveal>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="tb-modal-logo">
        <img src="<?php echo JURI::base() . 'templates/' . $app->getTemplate() . '/'; ?>img/modal-logo.png" alt="">
    </div>
    <div class="row">
        <div class="small-12 columns">
            <button class="button">Sign up with facebook</button>
        </div>
        <div class="small-12 columns">
            <button class="button alert">Sign up with Google+</button>
        </div>
        <div class="small-12 columns">
            OR
        </div>
        <form action="#" class="tb-form clearfix">
            <div class="small-12 columns">
                <input type="text" placeholder="First name">
            </div>
            <div class="small-12 columns">
                <input type="text" placeholder="Last name">
            </div>
            <div class="small-12 columns">
                <input type="text" placeholder="Email">
            </div>
            <div class="small-12 columns">
                <input type="text" placeholder="Password">
            </div>

            <div class="small-12 columns">
                <input type="checkbox" id="acceptCheckbox">I would like to <a href="#">receive coupon and
                    inspiration</a></a>
            </div>
            <div class="small-12 columns text-center">
                By signing up, I agree to triipbook's <a href="#">Term of Service and Privacy Policy.</a>
            </div>
            <div class="small-12 columns">
                <button type="button" name="button" class="button success login-button">sign up</button>
                <hr/>
            </div>
        </form>
        <div class="small-12 columns text-center">
            Already a triipbook member ? <a href="#" data-toggle="loginModal">Login</a>
        </div>
    </div>
</div>
<!-- profileModal -->


<script src="<?php echo $path ?>bower_components/jquery/dist/jquery.js"></script>
<script src="<?php echo $path ?>bower_components/what-input/what-input.js"></script>
<script src="<?php echo $path ?>bower_components/foundation-sites/dist/foundation.js"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="<?php echo $path ?>js/app.js"></script>
</body>
