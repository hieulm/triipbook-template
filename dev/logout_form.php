<?php
/**
 * Created by PhpStorm.
 * User: hieulm
 * Date: 2/1/16
 * Time: 6:31 PM
 */
<form action = "<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method = "post" id = "login-form" >

        <input type = "submit" name = "Submit" class="button" value = "<?php echo JText::_('JLOGOUT'); ?>" />
        <input type = "hidden" name = "option" value = "com_users" />
        <input type = "hidden" name = "task" value = "user.logout" />
        <input type = "hidden" name = "return" value = "<?php echo $return; ?>" />
        <?php echo JHtml::_('form.token'); ?>
</form>